let express = require('express');
let app = express();

const RedisDBHelper = require("./redis/redisHelper");
let redis = new RedisDBHelper();

const fs = require('fs-extra');


app.get('/send', notifyAllUsers);

async function notifyAllUsers(req, res) {
    const messageTemplate = req.query.template;
    if (messageTemplate) {
        await redis.cleanPendingRequests();
        const wasPreviousFillInterrupted = await redis.wasPreviousFillInterrupted();
        const wasAllPlayersNotified = await redis.wasAllPlayersNotified();
        if (wasPreviousFillInterrupted || wasAllPlayersNotified) {
            const hasUsernameInMessageTemplate = messageTemplate.includes("{user_name}");
            await redis.flushAndfillFromMysql(hasUsernameInMessageTemplate);
        }
        await notifyPlayersGroups(messageTemplate);
        console.log("notify finished");
        res.send("Notification process started!");
    } else {
        res.send("Empty template!")
    }
}

async function notifyPlayersGroups(messageTemplate) {
    let vkServerNotifyError = false, finished = false;
    while (!vkServerNotifyError && !finished) {
        let name, ids;
        ({name, ids, finished} = await redis.iterateNextPlayersGroup());
        if (ids.length > 0) {
            sendNotificationAndLog(name, ids, messageTemplate)
                .then(() => {
                    console.log("resolved", name);
                    redis.resolvePlayersGroup(name, ids);
                })
                .catch(err => {
                    console.log("sendNotify promise error: ", err);
                    if (err.code !== 1) {
                        vkServerNotifyError = true;
                    }
                    redis.rejectPlayersGroup(name, ids);
                });
        }
        await delay(350);
    }
}

function delay(t, val) {
    return new Promise(function (resolve) {
        setTimeout(function () {
            resolve(val);
        }, t);
    });
}

function sendNotificationAndLog(name, ids, messageTemplate) {
    const textMessage = messageTemplate.replace(new RegExp("{user_name}", 'g'), name);
    return promiseWithTimeout(sendNotify(ids, textMessage))
        .then(logNotifiedPlayers());
}

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});

function sendNotify(ids, text) {
    return new Promise((resolve, _) => {
        setTimeout(() => resolve(ids), 1000);
    })
}


function promiseWithTimeout(promise, timeout = 10000) {
    return Promise.race([
        promise,
        new Promise((_, reject) =>
            setTimeout(() => reject({code: 1, description: "vk api request timeout"}), timeout)
        )
    ]);
}


function logNotifiedPlayers() {
    return async function (ids) {
        const logFilename = __dirname + "/log.txt";
        try {
            await fs.appendFile(logFilename, `\nids notified: [${ids}]`);
        } catch (err) {
            console.log("writting log file err: ", err);
        }
        return ids;
    }
}
