let mysql = require('promise-mysql');
let Promise = require("bluebird");
let mysqlConfig = require("./mysqlConfig");

let conPool = mysql.createPool(mysqlConfig);

module.exports.inquirePlayers = function (poolSize, page) {
    return Promise.using(getSqlConnection(), function (connection) {
        const sql = `select id, name from players limit ${poolSize} offset ${poolSize * page}`;
        return connection.query(sql).then(result => {
            return result.map(mysqlObject => {
                return {id: mysqlObject.id, name: mysqlObject.name}
            });
        })
    });
};

function getSqlConnection() {
    return conPool.getConnection().disposer(function (connection) {
        conPool.releaseConnection(connection);
    });
}