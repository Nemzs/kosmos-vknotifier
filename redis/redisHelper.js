const mysqlHelper = require("../mysql/mysqlHelper");
const RedisUtils = require("./redisUtils");
const redis = new RedisUtils();

module.exports = function RedisDBHelper() {

    this.flushAndfillFromMysql = async function (hasUsernameInMessageTemplate) {
        await redis.flushallAsync();

        let filled = 0;
        let page = 0;
        do {
            filled = await getPlayers(page++, hasUsernameInMessageTemplate);
            console.log("Players loaded: ", filled);
        } while (filled > 0);
        return 0;
    };


    async function getPlayers(page, hasUsernameInMessageTemplate) {
        const addPool = 10000;
        const players = await mysqlHelper.inquirePlayers(addPool, page);
        players.forEach(async function (player) {
            if (hasUsernameInMessageTemplate) {
                await redis.saddAsync(player.name, player.id)
            } else {
                await redis.saddAsync("sharedPool", player.id)
            }
        });
        return players.length;
    }

    this.wasPreviousFillInterrupted = async function () {
        const notifiedPlayersExists = await redis.existsAsync("notified");
        return !notifiedPlayersExists;
    };

    this.wasAllPlayersNotified = async function () {
        const notifiedPlayersExists = await redis.existsAsync("notified");
        const redisKeysCount = await redis.dbsizeAsync();
        return notifiedPlayersExists && (redisKeysCount === 1);
    };

    this.iterateNextPlayersGroup = async function () {
        const namesChunk = await scanNamesChunk();
        if (namesChunk.names.length > 0) {
            const playerName = namesChunk.names[0];
            const ids = await redis.spopAsync(playerName, 100);
            const idsWithNames = ids.map(id => id + "_" + playerName);
            await redis.saddAsync("pending", idsWithNames);
            return {name: namesChunk.names[0], ids: ids, finished: false};
        } else {
            return {name: "", ids: [], finished: !namesChunk.hasPendingIds};
        }
    };

    async function scanNamesChunk() {
        let scanToken = "0", playerNames = [];
        let playerNamesFiltered = [];
        do {
            [scanToken, playerNames] = await redis.scanAsync(scanToken);
            playerNamesFiltered = playerNames.filter(name => (name !== 'notified' && name !== 'pending'));
        } while (scanToken !== '0' && playerNamesFiltered.length === 0);

        const pendingKey = playerNames.includes("pending");
        return {names: playerNamesFiltered, hasPendingIds: pendingKey};
    }

    this.resolvePlayersGroup = async function (name, ids) {
        const idsWithName = ids.map(id => id + "_" + name);
        await redis.sremAsync("pending", idsWithName);
        await redis.saddAsync("notified", ids);

        return 0;
    };
    this.rejectPlayersGroup = async function (name, ids) {
        const idsWithName = ids.map(id => id + "_" + name);
        await redis.sremAsync("pending", idsWithName);
        await redis.saddAsync(name, ids);
        return 0;
    };

    this.cleanPendingRequests = async function () {
        const pendingPlayers = await redis.smembersAsync("pending");
        pendingPlayers.forEach(idName => {
            const index = idName.indexOf("_");
            const id = idName.substr(0, index);
            const name = idName.substr(index + 1);
            redis.saddAsync(name, id);
        });
    }
};