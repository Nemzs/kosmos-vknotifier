let redis = require("redis");
const {promisify} = require('util');

let client = redis.createClient();

client.on("error", function (err) {
    console.log("Redis server error: " + err);
});

module.exports = function RedisUtils() {
    function promisifyRedisWrapper(func, client) {
        return promisify(func).bind(client);
    }

    this.scanAsync = promisifyRedisWrapper(client.scan, client);
    this.saddAsync = promisifyRedisWrapper(client.sadd, client);
    this.spopAsync = promisifyRedisWrapper(client.spop, client);
    this.smoveAsync = promisifyRedisWrapper(client.smove, client);
    this.smembersAsync = promisifyRedisWrapper(client.smembers, client);
    this.existsAsync = promisifyRedisWrapper(client.exists, client);
    this.dbsizeAsync = promisifyRedisWrapper(client.dbsize, client);
    this.flushallAsync = promisifyRedisWrapper(client.flushall, client);
    this.sremAsync = promisifyRedisWrapper(client.srem, client);
};